/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.cemagref.ohoui.ui.swing;

import fr.cemagref.ohoui.ui.swing.actions.AddElementInCollection;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

/**
 * Listener that is responsible to trigger a popup menu when a right-click is
 * performed on a node.
 * 
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class NodePopupListener extends MouseAdapter {
       private JPopupMenu popup;

        public NodePopupListener(NodesAndPanelsPanel panel) {
            super();
            //Create the popup menu.
            popup = new JPopupMenu();
            // TODO add actions dependending on the selected element
            JMenuItem menuItem = new JMenuItem(new AddElementInCollection(panel));
            popup.add(menuItem);
            // TODO add a submenu that displays types available for adding a new element
        }

        @Override
        public void mousePressed(MouseEvent e) {
            maybeShowPopup(e);
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            maybeShowPopup(e);
        }

        private void maybeShowPopup(MouseEvent e) {
            if (e.isPopupTrigger()) {
                popup.show(e.getComponent(),
                        e.getX(), e.getY());
            }
        }

}
