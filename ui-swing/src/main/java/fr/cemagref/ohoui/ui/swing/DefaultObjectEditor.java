/*
 *  Copyright (C) 2010 Cemagref
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.ohoui.ui.swing;

import fr.cemagref.ohoui.structure.OhObject;
import fr.cemagref.ohoui.structure.OhObjectComplex;

import fr.cemagref.ohoui.ui.swing.basiccontrols.AbstractOhEditor;
import fr.cemagref.ohoui.ui.swing.basiccontrols.CommonDefaultObjectEditor;
import fr.cemagref.ohoui.ui.swing.basiccontrols.EditorsProvider;
import javax.swing.JComponent;
import javax.swing.JPanel;

/**
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class DefaultObjectEditor extends CommonDefaultObjectEditor {

    public DefaultObjectEditor(OhObjectComplex<?> object) {
        super(object);
    }

    @Override
    protected void addEditorFor(OhObject object, int index) {
        AbstractOhEditor<Object, JComponent> editor = EditorsProvider.createEditorFor(object);
        if (editor != null) {
            // TODO add button for reinstanciate object
            addPanelAt((JComponent) editor.getPanel(), index);
            registerEditor(editor);
        } else {
            // when a null collection or complex child is instancied, we have no editor
            // TODO notify the tree if a list or complex object is added for example
            addPanelAt(new JPanel(), index);
        }
    }
}
