/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.cemagref.ohoui.ui.swing;

import fr.cemagref.ohoui.structure.OhObjectCollection;
import fr.cemagref.ohoui.structure.OhObjectComplex;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class OhCollectionTreeNode extends OhComplexTreeNode<OhObjectCollection> {

    public OhCollectionTreeNode(OhComplexTreeNode parent, OhObjectCollection ohObjectComplex) {
        super(parent, ohObjectComplex);
    }

    @Override
    protected OhObjectComplex getOhChildAt(int childIndex) {
        return (OhObjectComplex) this.content.getChildren().get(childIndex);
    }

    @Override
    public int getChildCount() {
        return this.content.getChildren().size();
    }

    @Override
    public int getIndex(OhComplexTreeNode node) {
        return this.content.getChildren().indexOf(node);
    }


}
