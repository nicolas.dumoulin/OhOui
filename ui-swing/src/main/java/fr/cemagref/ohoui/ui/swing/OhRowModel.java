/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.cemagref.ohoui.ui.swing;

import org.netbeans.swing.outline.RowModel;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class OhRowModel implements RowModel {

    @Override
    public int getColumnCount() {
        return 0;
    }

    @Override
    public Object getValueFor(Object node, int column) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Class getColumnClass(int column) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean isCellEditable(Object node, int column) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void setValueFor(Object node, int column, Object value) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String getColumnName(int column) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
