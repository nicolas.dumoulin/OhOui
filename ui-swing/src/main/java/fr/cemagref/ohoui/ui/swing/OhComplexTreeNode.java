/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.ohoui.ui.swing;

import fr.cemagref.ohoui.structure.OhObject;
import fr.cemagref.ohoui.structure.OhObjectCollection;
import fr.cemagref.ohoui.structure.OhObjectComplex;
import fr.cemagref.ohoui.ui.swing.basiccontrols.EditorsProvider;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class OhComplexTreeNode<T extends OhObjectComplex<?>> {

    private OhComplexTreeNode parent;
    protected T content;
    private List<OhObjectComplex> complexChilds;

    public OhComplexTreeNode(OhComplexTreeNode parent, T ohObjectComplex) {
        this.content = ohObjectComplex;
        this.complexChilds = new ArrayList<OhObjectComplex>();
        for (OhObject object : ohObjectComplex) {
            if (((object instanceof OhObjectComplex) && !(EditorsProvider.hasEditorFor(object))) || (ohObjectComplex instanceof OhObjectCollection)) {
                complexChilds.add((OhObjectComplex) object);
            }
        }
    }

    public T getContent() {
        return content;
    }

    public String getName() {
        return content.getName();
    }

    protected OhObjectComplex getOhChildAt(int childIndex) {
        return complexChilds.get(childIndex);
    }

    public OhComplexTreeNode getChildAt(int childIndex) {
        OhObjectComplex child = getOhChildAt(childIndex);
        if (child instanceof OhObjectCollection) {
            return new OhCollectionTreeNode(this, (OhObjectCollection) child);
        } else {
            return new OhComplexTreeNode(this, child);
        }
    }

    public int getChildCount() {
        return complexChilds.size();
    }

    public OhComplexTreeNode getParent() {
        return parent;
    }

    public int getIndex(OhComplexTreeNode node) {
        return complexChilds.indexOf(((OhComplexTreeNode) node).content);
    }
}
