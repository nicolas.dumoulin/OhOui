/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * NodesAndPanelsPanel.java
 *
 * Created on 16 févr. 2010, 14:26:15
 */
package fr.cemagref.ohoui.ui.swing;

import fr.cemagref.ohoui.filters.FieldFilter;
import fr.cemagref.ohoui.reflect.DefaultObjectIntrospector;
import fr.cemagref.ohoui.reflect.IntrospectionException;
import fr.cemagref.ohoui.reflect.OhOuiContext;
import fr.cemagref.ohoui.structure.OhObject;
import fr.cemagref.ohoui.structure.OhObjectCollection;
import fr.cemagref.ohoui.structure.OhObjectComplex;
import fr.cemagref.ohoui.ui.swing.basiccontrols.AbstractOhEditor;
import fr.cemagref.ohoui.ui.swing.basiccontrols.EditorsProvider;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.netbeans.swing.outline.DefaultOutlineModel;
import org.netbeans.swing.outline.Outline;
import org.netbeans.swing.outline.OutlineModel;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class NodesAndPanelsPanel extends JPanel implements ListSelectionListener {

    private static final Logger LOGGER = Logger.getLogger(NodesAndPanelsPanel.class.getName());
    private final static Dimension jTableMinSize = new Dimension(150, 100);
    private final static Dimension jTableMaxSize = new Dimension(32767, 32767);
    private final static Dimension jTablePreferredSize = new Dimension(200, 400);
    private final static Dimension editorMinSize = new Dimension(200, 100);
    private final static Dimension editorMaxSize = new Dimension(32767, 32767);
    private final static Dimension editorPreferredSize = new Dimension(200, 400);
    private OhOuiContext ohOuiContext;
    private List<FieldFilter> fieldFilters;
    private Outline outline;
    private ListSelectionModel selectionModel;
    private OutlineModel outlineModel;
    private OhTreeModel treeModel;
    private Map<Object, AbstractOhEditor<Object, JComponent>> editorsMap;
    private CardLayout editorsCardLayout;
    private final static String EMPTY_EDITOR_CODE = "empty";

    /** Creates new form NodesAndPanelsPanel */
    public NodesAndPanelsPanel(Object o) throws IntrospectionException {
        ohOuiContext = new OhOuiContext();
        fieldFilters = new ArrayList<FieldFilter>();
        // build data model
        OhObjectComplex ohObjectComplex = DefaultObjectIntrospector.introspect("test", o, ohOuiContext, getFieldFiltersArray());
        treeModel = new OhTreeModel(ohObjectComplex);
        outlineModel = DefaultOutlineModel.createOutlineModel(treeModel, new OhRowModel());
        // build form and adjust UI settings
        initComponents();
        jSplitPane1.setPreferredSize(new Dimension(jTablePreferredSize.width + editorPreferredSize.width, Math.max(jTablePreferredSize.height, editorPreferredSize.height)));
        jSplitPane1.setDividerLocation(jTablePreferredSize.width);
        jTable.setPreferredScrollableViewportSize(jTable.getPreferredSize());
        // adjust the table renderer and listener
        outline = (Outline) jTable;
        outline.setRenderDataProvider(new OhDataProvider());
        selectionModel = outline.getSelectionModel();
        outline.getSelectionModel().addListSelectionListener(this);
        outline.addMouseListener(new NodePopupListener(this));
        // init editors
        editorsMap = new HashMap<Object, AbstractOhEditor<Object, JComponent>>();
        editorsCardLayout = (CardLayout) editorsPanel.getLayout();
        editorsPanel.add(new JPanel(), EMPTY_EDITOR_CODE);
        // select the root
        selectionModel.setSelectionInterval(0, 0);
    }

    public OhComplexTreeNode getSelectedElement() {
        int selectedRow = selectionModel.getMinSelectionIndex();
        return (OhComplexTreeNode) outlineModel.getValueAt(selectedRow, 0);
    }

    public Outline getOutline() {
        return outline;
    }

    public OhTreeModel getTreeModel() {
        return treeModel;
    }

    public OhOuiContext getOhOuiContext() {
        return ohOuiContext;
    }

    public final FieldFilter[] getFieldFiltersArray() {
        return fieldFilters.toArray(new FieldFilter[]{});
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()) {
            return;
        }
        if (selectionModel.isSelectionEmpty()) {
            // clear right panel
            editorsCardLayout.show(editorsPanel, EMPTY_EDITOR_CODE);
            LOGGER.fine("No selection");
        } else {
            LOGGER.fine("Selection : " + selectionModel.getMinSelectionIndex() + " - " + getSelectedElement());
            OhObject selectedObject = getSelectedElement().getContent();
            if (editorsMap.containsKey(selectedObject)) {
                // editor is already in cache
                editorsCardLayout.show(editorsPanel, getCode(selectedObject));
            } else {
                // retrieve editor and put it in cache
                AbstractOhEditor<Object, JComponent> editor = null;
                if (EditorsProvider.hasEditorFor(selectedObject)) {
                    editor = EditorsProvider.createEditorFor(selectedObject);
                    // TODO following tests should be avoided by presence of correct producer
                } else if (selectedObject instanceof OhObjectCollection) {
                    // TODO bring an editor with a list and add/remove buttons
                } else if (selectedObject instanceof OhObjectComplex) {
                    editor = new DefaultObjectEditor((OhObjectComplex<?>) selectedObject);
                    ((DefaultObjectEditor) editor).buildEditor();
                } else {
                    // TODO pay attention if object value is null ?
                    editor = EditorsProvider.createEditorFor(selectedObject);
                }
                editorsMap.put(selectedObject, editor);
                if (editor != null) {
                    editorsPanel.add(editor.getPanel(), getCode(selectedObject));
                    editorsCardLayout.show(editorsPanel, getCode(selectedObject));
                } else {
                    editorsCardLayout.show(editorsPanel, EMPTY_EDITOR_CODE);
                }
            }
        }
        editorsPanel.revalidate();
    }

    private String getCode(OhObject selectedObject) {
        return Integer.toString(selectedObject.hashCode());
    }

    /**
     * Apply all the modifications done in the forms to the edited object.
     * @return the object edited after applying all the modifications.
     */
    public Object applyChanges() {
        // the DefaultObjectEditor will recursively apply changes
        // FIXME how to manage objects that appears in tree (skipped because of
        //  EditorsProvider.hasEditorFor(child) in DefaultObjectEditor constructor)
        OhObjectComplex rootOhObjectComplex = ((OhComplexTreeNode) treeModel.getRoot()).getContent();
        AbstractOhEditor<Object, JComponent> rootEditor = editorsMap.get(rootOhObjectComplex);
        return rootEditor.getValue();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSplitPane1 = new javax.swing.JSplitPane();
        jScrollPaneRight = new javax.swing.JScrollPane();
        editorsPanel = new javax.swing.JPanel();
        jScrollPaneLeft = new javax.swing.JScrollPane();
        jTable = new Outline();

        jSplitPane1.setDividerSize(6);
        jSplitPane1.setResizeWeight(0.1);

        jScrollPaneRight.setMaximumSize(editorMaxSize);
        jScrollPaneRight.setMinimumSize(editorMinSize);
        jScrollPaneRight.setPreferredSize(editorPreferredSize);

        editorsPanel.setLayout(new java.awt.CardLayout());
        jScrollPaneRight.setViewportView(editorsPanel);

        jSplitPane1.setRightComponent(jScrollPaneRight);

        jScrollPaneLeft.setMaximumSize(jTableMaxSize);
        jScrollPaneLeft.setMinimumSize(jTableMinSize);
        jScrollPaneLeft.setPreferredSize(jTablePreferredSize);

        jTable.setModel(outlineModel);
        jTable.setMaximumSize(jTableMaxSize);
        jTable.setMinimumSize(jTableMinSize);
        jTable.setPreferredSize(jTablePreferredSize);
        jScrollPaneLeft.setViewportView(jTable);

        jSplitPane1.setLeftComponent(jScrollPaneLeft);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 707, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 454, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel editorsPanel;
    private javax.swing.JScrollPane jScrollPaneLeft;
    private javax.swing.JScrollPane jScrollPaneRight;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JTable jTable;
    // End of variables declaration//GEN-END:variables
}
