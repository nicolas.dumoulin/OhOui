/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.ohoui.ui.swing.actions;

import fr.cemagref.ohoui.reflect.CollectionIntrospector;
import fr.cemagref.ohoui.reflect.IntrospectionException;
import fr.cemagref.ohoui.structure.OhObjectCollection;
import fr.cemagref.ohoui.ui.swing.NodesAndPanelsPanel;
import fr.cemagref.ohoui.ui.swing.OhComplexTreeNode;
import java.awt.event.ActionEvent;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.JOptionPane;
import org.objenesis.ObjenesisHelper;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class AddElementInCollection extends AbstractAction {

    private NodesAndPanelsPanel panel;

    public AddElementInCollection(NodesAndPanelsPanel panel) {
        super("Add an element");
        this.panel = panel;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        // TODO implement and then extract generic part in OhComplexTreeNode?
        OhComplexTreeNode node = panel.getSelectedElement();
        assert node.getContent() instanceof OhObjectCollection;
        OhObjectCollection ohCollection = (OhObjectCollection) node.getContent();
        // Editors retrieving for other types
        List<Class> availableTypes = new ArrayList<Class>();
        Class baseType = ohCollection.getDeclaredItemClass();
        availableTypes.add(baseType);
        availableTypes.addAll(Lookup.getDefault().lookupResult(baseType).allClasses());
        // remove abstract classes
        for (Iterator<Class> it = availableTypes.iterator(); it.hasNext();) {
            Class type = it.next();
            if ((type.isInterface()) || (Modifier.isAbstract(type.getModifiers()))) {
                it.remove();
            }
        }
        // select the type
        Class chosenType;
        if (availableTypes.isEmpty()) {
            JOptionPane.showMessageDialog(panel, "It is impossible to add a new "
                    + "element: No implementation of the type "
                    + baseType + " has been configured",
                    "Creation not possible", JOptionPane.ERROR_MESSAGE);
            // TODO propose to try to find an implementation
            return;
        } else if (availableTypes.size() == 1) {
            chosenType = availableTypes.get(0);
        } else {
            // bring a dialog to choose the type
            chosenType = (Class) JOptionPane.showInputDialog(panel,
                    "Choose the type you want to use", "Choose a type", JOptionPane.QUESTION_MESSAGE, null,
                    availableTypes.toArray(new Class[]{}), availableTypes.get(0));
        }
        if (chosenType != null) {
            try {
                Object newInstance = ObjenesisHelper.newInstance(chosenType);
                // instanciate and add in the collection
                ohCollection.getValue().add(newInstance);
                CollectionIntrospector.addChild(newInstance, ohCollection, panel.getOhOuiContext(), ohCollection.getValue().size() - 1, panel.getFieldFiltersArray());
                // notify the tree
                panel.getTreeModel().addChild();
                // doesn't work:
                // panel.getOutline().tableChanged(new TableModelEvent(panel.getOutline().getOutlineModel()));
                // panel.getOutline().revalidate();
            } catch (IntrospectionException ex) {
                Exceptions.printStackTrace(ex);
            } catch (IllegalArgumentException ex) {
                Exceptions.printStackTrace(ex);
            }
        }

    }
}
