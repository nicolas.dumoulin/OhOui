/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.cemagref.ohoui.reflect;

import fr.cemagref.ohoui.annotations.Description;
import fr.cemagref.ohoui.filters.NoTransientField;
import fr.cemagref.ohoui.structure.OhObject;
import fr.cemagref.ohoui.structure.OhObjectComplex;
import junit.framework.TestCase;

/**
 *
 * @author dumoulin
 */
public class DefaultObjectIntrospectorTest extends TestCase {

    private static final String MY_OBJECT = "My object";

    public DefaultObjectIntrospectorTest(String testName) {
        super(testName);
    }

    public void testIntrospect() throws Exception {
        System.out.println("introspect");
        MyClass object = new MyClass();
        OhObjectComplex result = DefaultObjectIntrospector.introspect(MY_OBJECT, object, new OhOuiContext());
        assertEquals(MY_OBJECT, result.getName());
        assertEquals(3, result.getChildren().size());
        // TODO Why this cast is needed? Without it, compiler complains that getName() method doesn't exist for class Object!
        assertEquals("i", ((OhObject)result.getChildren().get(1)).getName());
        assertEquals(object.i, ((OhObject)result.getChildren().get(1)).getValue());
        assertEquals(MyClass.ANNOTATION, ((OhObject)result.getChildren().get(2)).getName());
        assertEquals(object.d, ((OhObject)result.getChildren().get(2)).getValue());
        // Test field filter application
        assertEquals(2, DefaultObjectIntrospector.introspect(MY_OBJECT, object, new OhOuiContext(), new NoTransientField()).getChildren().size());
    }

    static class MyClass {

        transient static final String ANNOTATION = "My double";
        int i = 1;
        @Description(name = ANNOTATION)
        double d = 0.5;
    }
}

