/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.cemagref.ohoui.reflect;

import fr.cemagref.ohoui.annotations.Description;
import fr.cemagref.ohoui.filters.NoTransientField;
import fr.cemagref.ohoui.structure.OhObjectCollection;
import fr.cemagref.ohoui.structure.OhObjectComplex;
import java.util.ArrayList;
import java.util.List;
import junit.framework.TestCase;

/**
 *
 * @author dumoulin
 */
public class CollectionIntrospectorTest extends TestCase {

    private static final String MY_OBJECT = "My object";

    public CollectionIntrospectorTest(String testName) {
        super(testName);
    }

    public void testIntrospect() throws Exception {
        System.out.println("introspect");
        MyClass object = new MyClass();
        OhObjectComplex result = DefaultObjectIntrospector.introspect("test", object, new OhOuiContext());
        // one child should be found
        assertEquals(1, result.getChildren().size());
        // this child should be a collection handler
        assertEquals(OhObjectCollection.class, result.getChildren().get(0).getClass());
        // this collection should have 2 children
        assertEquals(2, ((OhObjectCollection) result.getChildren().get(0)).getChildren().size());
        assertEquals(3, ((OhObjectCollection) result.getChildren().get(0)).getChildren().get(0).getValue());
    }

    static class MyClass {

        List<Number> myList = new ArrayList<Number>();

        public MyClass() {
            myList.add(3);
            myList.add(0.5);
        }
    }
}

