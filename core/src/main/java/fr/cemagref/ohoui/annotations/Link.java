package fr.cemagref.ohoui.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <code>Link</code> Allow to link action to another field.
 * 
 * For example the action "disable" on a boolean will disable the OUIPanel
 * targeted when the checkbox corresponding to the boolean field will be
 * checked. To set the target,
 * 
 * @see fr.cemagref.ohoui.annotations.Anchor
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Link {
    String action();

    String target();
}
