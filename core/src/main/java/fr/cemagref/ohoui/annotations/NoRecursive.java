package fr.cemagref.ohoui.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <code>NoRecursive</code> This annotation is used to decide if the
 * introspection process should inpect recursively superClass.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface NoRecursive {

}
