package fr.cemagref.ohoui.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <code>Anchor</code>
 * 
 * @see fr.cemagref.ohoui.annotations.Link
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Anchor {
    String id();
}
