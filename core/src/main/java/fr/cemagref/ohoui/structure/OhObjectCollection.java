/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.cemagref.ohoui.structure;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author dumoulin
 */
public class OhObjectCollection extends OhObjectComplex<Collection> {

    private Type genericType;
    private Class declaredItemClass;

    public OhObjectCollection(String name, Class declaredType, Type genericType, Collection value) {
        super(name, declaredType, value);
        this.genericType = genericType;
    }

    public OhObjectCollection(String name, Collection value) {
        super(name, value);
    }

    public OhObjectCollection(String name, Collection value, List<OhObject> children) {
        super(name, value, children);
    }

    public Type getGenericType() {
        return genericType;
    }

    public Class getDeclaredItemClass() {
        return declaredItemClass;
    }

    public void setDeclaredItemClass(Class declaredItemClass) {
        this.declaredItemClass = declaredItemClass;
    }
}
