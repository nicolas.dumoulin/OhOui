/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.cemagref.ohoui.structure;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author dumoulin
 */
public class OhObjectComplex<T> extends OhObject<T> implements Iterable<OhObject> {

    private List<OhObject> children = new ArrayList<OhObject>();

    public OhObjectComplex(String name, T value, List<OhObject> children) {
        super(name, value);
        this.children = children;
    }

    public OhObjectComplex(String name, T value) {
        super(name, value);
    }

    public OhObjectComplex(String name, Class declaredType, T value) {
        super(name, declaredType, value);
    }

    public List<OhObject> getChildren() {
        return children;
    }

    public boolean addChild(OhObject e) {
        e.setParent(this);
        return children.add(e);
    }

    @Override
    public Iterator<OhObject> iterator() {
        return children.iterator();
    }
}
