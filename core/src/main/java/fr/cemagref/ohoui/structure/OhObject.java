package fr.cemagref.ohoui.structure;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.logging.Logger;

public class OhObject<T> {

    protected String name;
    protected String description;
    protected Class declaredType;
    protected T value;
    protected OhObjectComplex parent;
    protected Field field;

    public OhObject(String name, Class declaredType, T value) {
        this.name = name;
        this.declaredType = declaredType;
        this.value = value;
    }

    public OhObject(String name, T value) {
        this(name, value.getClass(), value);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Class getDeclaredType() {
        return declaredType;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) throws IllegalArgumentException, IllegalAccessException {
        this.value = value;
        if (parent != null) {
            field.set(parent.getValue(), value);
        } else {
            Logger.getLogger(OhObject.class.getName()).warning("Parent not present for object: " + this.name);
        }
    }

    public Field getField() {
        return field;
    }

    public void setField(Field field) {
        this.field = field;
    }

    public OhObjectComplex getParent() {
        return parent;
    }

    public void setParent(OhObjectComplex parent) {
        this.parent = parent;
    }

    public boolean isFinal() {
        return Modifier.isFinal(field.getModifiers());
    }

    @Override
    public String toString() {
        StringBuilder buf = new StringBuilder(getName());
        buf.append(" (").append(getDeclaredType());
        buf.append(") = ").append(getValue());
        return buf.toString();
    }

}
