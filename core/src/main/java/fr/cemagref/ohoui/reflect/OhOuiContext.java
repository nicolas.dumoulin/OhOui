package fr.cemagref.ohoui.reflect;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class OhOuiContext {

    private Stack<Object> visitedObjects;
    private List<Class> leafTypes;

    public OhOuiContext() {
        this(new ArrayList<Class>());
    }

    public OhOuiContext(List<Class> leafTypes) {
        this.visitedObjects = new Stack<Object>();
        this.leafTypes = leafTypes;
    }

    public Stack<Object> getVisitedObjects() {
        return visitedObjects;
    }

    public boolean addLeafType(Class e) {
        return leafTypes.add(e);
    }

    /**
     * Says if a type should be considered as a leaf, and
     * if the introspection process should stops and not go
     * deeper. Primitive types are always considered as leaf.
     * @param type
     * @return
     */
    public boolean isLeaf(Class type) {
        return type.isPrimitive() ? true : leafTypes.contains(type);
    }

}
