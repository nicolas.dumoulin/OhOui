package fr.cemagref.ohoui.reflect;

import fr.cemagref.ohoui.annotations.Description;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import fr.cemagref.ohoui.annotations.NoRecursive;
import fr.cemagref.ohoui.filters.FieldFilter;
import fr.cemagref.ohoui.structure.OhObject;
import fr.cemagref.ohoui.structure.OhObjectCollection;
import fr.cemagref.ohoui.structure.OhObjectComplex;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.logging.Logger;

public class DefaultObjectIntrospector {

    static OhObject introspectChild(OhOuiContext ohOui, Class fieldType, String fieldName, Object fieldValue, Type fieldGenericType, FieldFilter[] fieldFilters) throws IntrospectionException {
        OhObject child;
        if (ohOui.isLeaf(fieldType)) {
            child = new OhObject(fieldName, fieldType, fieldValue);
        } else if (Collection.class.isAssignableFrom(fieldType)) {
            child = new OhObjectCollection(fieldName, fieldType, fieldGenericType, (Collection) fieldValue);
            CollectionIntrospector.introspect((OhObjectCollection) child, ohOui, fieldFilters);
        } else {
            child = new OhObjectComplex(fieldName, fieldType, fieldValue);
            introspect((OhObjectComplex) child, ohOui, fieldFilters);
        }
        return child;
    }

    private static String buildNameFromField(Field field) {
        String name = field.getName();
        if (field.isAnnotationPresent(Description.class)) {
            name = field.getAnnotation(Description.class).name();
        }
        return name;
    }

    /**
     * Starts the introspection of an object
     * @param name the name identifying the object
     * @param object the object to introspect
     * @param ohOui the context
     * @param fieldFilters the filters to use for the introspection
     * @return the root of the data fetched during the introspection
     * @throws IntrospectionException
     */
    public static OhObjectComplex introspect(String name, Object object, OhOuiContext ohOui, FieldFilter... fieldFilters) throws IntrospectionException {
        return introspect(new OhObjectComplex(name, object), ohOui, fieldFilters);
    }

    protected static OhObjectComplex introspect(OhObjectComplex objectComplex, OhOuiContext ohOui, FieldFilter... fieldFilters) throws IntrospectionException {
        // This object must not be parsed an other time
        if (ohOui.getVisitedObjects().search(objectComplex.getValue()) >= 0) {
            return null;
        }
        ohOui.getVisitedObjects().push(objectComplex.getValue());

        // And now, we begin the instrospection
        Class type = objectComplex.getDeclaredType();
        if (type == null) {
            if (objectComplex.getValue() == null) {
                Logger.getLogger(DefaultObjectIntrospector.class.getName()).warning("Can't introspect a null object!");
                return null;
            } else {
                type = objectComplex.getValue().getClass();
            }
        }
        // First, we fetch the declared fields of this object
        List<Field> fields = new ArrayList<Field>(Arrays.asList(type.getDeclaredFields()));
        // Then, we fetch fields of super-classes
        if (!type.isAnnotationPresent(NoRecursive.class)) {
            type = type.getSuperclass();
            while (type != null) {
                for (Field innerField : type.getDeclaredFields()) {
                    fields.add(innerField);
                }
                type = type.getSuperclass();
            }
        }
        fieldsLoop:
        for (Field innerField : fields) {
            if (!innerField.isAccessible()) {
                innerField.setAccessible(true);
            }
            // if one filter fails, we jump to the next field
            for (FieldFilter fieldFilter : fieldFilters) {
                if (!fieldFilter.accept(innerField)) {
                    continue fieldsLoop;
                }
            }
            try {
                String fieldName = buildNameFromField(innerField);
                Class fieldType = innerField.getType();
                Object fieldValue = null;
                if (objectComplex.getValue() != null) {
                    fieldValue = innerField.get(objectComplex.getValue());
                }
                OhObject child = introspectChild(ohOui, fieldType, fieldName, fieldValue, innerField.getGenericType(), fieldFilters);
                child.setField(innerField);
                if (innerField.isAnnotationPresent(Description.class)) {
                    child.setName(innerField.getAnnotation(Description.class).name());
                    child.setDescription(innerField.getAnnotation(Description.class).longDescription().trim());
                }
                objectComplex.addChild(child);
            } catch (IllegalArgumentException ex) {
                throw new IntrospectionException(ex);
            } catch (IllegalAccessException ex) {
                throw new IntrospectionException(ex);
            }
        }
        ohOui.getVisitedObjects().pop();
        return objectComplex;
    }
}
