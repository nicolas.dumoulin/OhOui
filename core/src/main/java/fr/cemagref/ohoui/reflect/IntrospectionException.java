package fr.cemagref.ohoui.reflect;

public class IntrospectionException extends Exception {

    public IntrospectionException(Throwable cause) {
        super(cause);
    }

    public IntrospectionException(String message, Throwable cause) {
        super(message, cause);
    }

}
