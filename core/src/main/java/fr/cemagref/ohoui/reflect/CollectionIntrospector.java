package fr.cemagref.ohoui.reflect;

import fr.cemagref.ohoui.filters.FieldFilter;
import fr.cemagref.ohoui.structure.OhObjectCollection;
import fr.cemagref.ohoui.structure.OhObjectComplex;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.Collection;

/**
 *
 * @author dumoulin
 */
public class CollectionIntrospector {

    protected static OhObjectComplex introspect(OhObjectCollection objectCollection, OhOuiContext ohOui, FieldFilter... fieldFilters) throws IntrospectionException {
        objectCollection.setDeclaredItemClass(getItemClass(objectCollection.getGenericType()));
        int i = 0;
        if (objectCollection.getValue() != null) {
            for (Object element : objectCollection.getValue()) {
                addChild(element, objectCollection, ohOui, i, fieldFilters);
                i++;
            }
        }
        return objectCollection;
    }

    /**
     * Add a child in a collection and introspect the object
     * @param element object to add
     * @param objectCollection the collection in which to add the child
     * @param ohOui the ohOui context handler
     * @param i the index of the child in the collection
     * @param fieldFilters
     * @throws IntrospectionException
     */
    public static void addChild(Object element, OhObjectCollection objectCollection, OhOuiContext ohOui, int i, FieldFilter[] fieldFilters) throws IntrospectionException {
        // invoke introspection on each element
        Type subCollectionGenericType = null;
        if (Collection.class.isAssignableFrom(element.getClass())) {
            subCollectionGenericType = getItemClass(objectCollection.getDeclaredItemClass());
        }
        objectCollection.addChild(DefaultObjectIntrospector.introspectChild(
                ohOui,
                objectCollection.getDeclaredItemClass(),
                "[" + i + "]",
                element,
                subCollectionGenericType,
                fieldFilters));
    }

    /**
     * <code>getItemClass</code> retrieves the class allowed for the items of
     * the field passed as argument. In fact, the class depends of the
     * collection declaration :
     * <ul>
     * <li>Collection : Object is returned</li>
     * <li>Collection<AClass> : AClass is returned</li>
     * <li>Collection<? extends AClass> or Collection<? super AClass> :
     * AClass is returned.</li>
     * </ul>
     */
    private static Class getItemClass(Type gtype) {
        Class result = Object.class;

        try {
            // if the type is parametrized, we look for this type
            if (gtype instanceof ParameterizedType) {
                // Collection are suposed to have only one parametrized type
                assert ((ParameterizedType) gtype).getActualTypeArguments().length == 1;
                Type type = ((ParameterizedType) gtype).getActualTypeArguments()[0];
                if (type instanceof WildcardType) {
                    // case with wildcard
                    if (((WildcardType) type).getLowerBounds().length > 0) {
                        result = (Class) ((WildcardType) type).getLowerBounds()[0];
                    } else if (((WildcardType) type).getUpperBounds().length > 0) {
                        result = (Class) ((WildcardType) type).getUpperBounds()[0];
                    }
                } else if (type instanceof TypeVariable) {
                    Type bound = ((TypeVariable) type).getBounds()[0];
                    if (bound instanceof ParameterizedType) {
                        // if the parameter is again parametrized
                        result = (Class) ((ParameterizedType) bound).getRawType();
                    } else if (bound instanceof Class) {
                        result = (Class) bound;
                    }
                } else if (type instanceof Class) // classical case
                {
                    result = (Class) type;
                }
            }
        } catch (Exception e) {
            // if error, fall to Object.class
        }
        return result;
    }
}
