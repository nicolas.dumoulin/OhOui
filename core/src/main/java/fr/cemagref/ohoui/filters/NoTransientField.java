package fr.cemagref.ohoui.filters;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class NoTransientField implements FieldFilter {

    public boolean accept(Field field) {
        return !Modifier.isTransient(field.getModifiers());
    }

}
