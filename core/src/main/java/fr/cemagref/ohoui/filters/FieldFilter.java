package fr.cemagref.ohoui.filters;

import java.lang.reflect.Field;

public interface FieldFilter {
    public boolean accept(Field field);
}
