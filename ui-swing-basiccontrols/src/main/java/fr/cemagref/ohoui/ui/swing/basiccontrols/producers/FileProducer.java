/*
 *  Copyright (C) 2010 Cemagref
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.ohoui.ui.swing.basiccontrols.producers;

import fr.cemagref.ohoui.structure.OhObject;
import fr.cemagref.ohoui.ui.swing.basiccontrols.ControlsFactory;
import fr.cemagref.ohoui.ui.swing.basiccontrols.OhEditorPanel;
import fr.cemagref.ohoui.ui.swing.basiccontrols.OhEditorProducer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JTextField;

public class FileProducer implements OhEditorProducer<File, JComponent> {

    private static Class[] ty = new Class[]{File.class};

    @Override
    public Class[] getCompatibleTypes() {
        return ty;
    }

    @Override
    public OhEditorPanel<File> getEditor(OhObject<File> object) {
        return new FileOUIPanel(object, object.getName(), object.getDescription());
    }

    @SuppressWarnings("serial")
    private class FileOUIPanel extends OhEditorPanel<File> implements ActionListener {

        private JTextField textField;
        private JFileChooser fileChooser;

        public FileOUIPanel(OhObject<File> object, String title, String tooltip) {
            super(object);
            this.fileChooser = new JFileChooser();
            textField = new JTextField(object.getValue().toString(), 20);
            this.addLabel(title, tooltip);
            this.addComponent(textField, tooltip);
            JButton browseButton = ControlsFactory.makeButton("general/Open16", this, "Browse", "...");
            panel.add(browseButton);
        }

        @Override
        public File getValue() {
            return new File(this.textField.getText());
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            this.fileChooser.setCurrentDirectory(new File(this.textField.getText()));
            if (this.fileChooser.showOpenDialog(panel) == JFileChooser.APPROVE_OPTION) {
                this.textField.setText(this.fileChooser.getSelectedFile().getAbsolutePath());
            }
        }
    }
}
