/*
 *  Copyright (C) 2010 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.ohoui.ui.swing.basiccontrols;

import fr.cemagref.ohoui.structure.OhObject;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComponent;
import org.openide.util.Lookup;

/**
 * Singleton class that provides graphical editors depending on the type
 * of the object to edit.
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class EditorsProvider {

    private static EditorsProvider instance;
    private static final Logger LOGGER = Logger.getLogger(EditorsProvider.class.getName());
    private Map<Class, OhEditorProducer> editorProducers;

    private EditorsProvider() {
        editorProducers = new HashMap<Class, OhEditorProducer>();
        for (OhEditorProducer producer : Lookup.getDefault().lookupAll(OhEditorProducer.class)) {
            for (Class type : producer.getCompatibleTypes()) {
                OhEditorProducer old = editorProducers.put(type, producer);
                if (old != null) {
                    StringBuilder buf = new StringBuilder("Editors conflicts for type ").append(type);
                    buf.append(", two editors found:\n   ").append(old).append(" (removed)\n   ");
                    buf.append(producer).append(" (retained)");
                    LOGGER.warning(buf.toString());
                }
            }
        }
        if (LOGGER.isLoggable(Level.INFO)) {
            StringBuilder buf = new StringBuilder("Producers loaded: ");
            for (Map.Entry<Class, OhEditorProducer> producer : editorProducers.entrySet()) {
                buf.append("   ").append(producer.getKey()).append(" -> ").append(producer.getValue()).append("\n");
            }
            LOGGER.info(buf.toString());
        }
    }

    private static EditorsProvider getDefault() {
        if (instance == null) {
            instance = new EditorsProvider();
        }
        return instance;
    }

    public static boolean hasEditorFor(OhObject ohObject) {
        if (ohObject.getValue() != null) {
            return getDefault().editorProducers.containsKey(ohObject.getValue().getClass());
        }
        return true;
    }

    public static AbstractOhEditor<Object, JComponent> createEditorFor(OhObject ohObject) {
        OhEditorProducer editor = null;
        if (ohObject.getValue() != null) {
            editor = getDefault().editorProducers.get(ohObject.getValue().getClass());
        }
        if (editor == null) {
            // TODO return an new DefaultObjectEditor ? should be avoided upstream
        }
        return (editor == null) ? null : editor.getEditor(ohObject);
    }
}
