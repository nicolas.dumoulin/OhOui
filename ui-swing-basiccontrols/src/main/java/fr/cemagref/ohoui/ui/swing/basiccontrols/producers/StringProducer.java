/*
 *  Copyright (C) 2010 Cemagref
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.ohoui.ui.swing.basiccontrols.producers;

import fr.cemagref.ohoui.structure.OhObject;
import fr.cemagref.ohoui.ui.swing.basiccontrols.OhEditorPanel;
import fr.cemagref.ohoui.ui.swing.basiccontrols.OhEditorProducer;
import fr.cemagref.ohoui.ui.swing.basiccontrols.SwingDefaults;

import javax.swing.JComponent;
import javax.swing.JTextField;

public class StringProducer implements OhEditorProducer<String, JComponent> {

    private static Class[] ty = new Class[]{String.class};

    @Override
    public Class[] getCompatibleTypes() {
        return ty;
    }

    @Override
    public OhEditorPanel getEditor(OhObject object) {
        final JTextField tf = new JTextField((String) object.getValue(), SwingDefaults.textFieldDefaultNumberOfChars);
        OhEditorPanel<String> panel = new OhEditorPanel<String>(object) {

            @Override
            public String getValue() {
                return tf.getText();
            }
        };
        panel.addLabel(object.getName(), object.getDescription());
        panel.addComponent(tf, object.getDescription());
        return panel;
    }
}
