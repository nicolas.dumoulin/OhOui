/*
 *  Copyright (C) 2010 Cemagref
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.ohoui.ui.swing.basiccontrols.producers;

import fr.cemagref.ohoui.structure.OhObject;
import fr.cemagref.ohoui.ui.swing.basiccontrols.OhEditorPanel;
import fr.cemagref.ohoui.ui.swing.basiccontrols.OhEditorProducer;
import fr.cemagref.ohoui.ui.swing.basiccontrols.SwingDefaults;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JComponent;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;

public class NumberProducer implements OhEditorProducer<Number, JComponent> {

    private static Class[] ty = new Class[]{byte.class, Byte.class, short.class, Short.class, int.class,
        Integer.class, long.class, Long.class, float.class, Float.class, double.class, Double.class};

    @Override
    public Class[] getCompatibleTypes() {
        return ty;
    }

    @Override
    public OhEditorPanel<Number> getEditor(final OhObject<Number> object) {
        final Class type = object.getValue().getClass();
        SpinnerModel spinnerModel = null;
        try {
            if (type.equals(byte.class) || type.equals(Byte.class)) {
                spinnerModel = new SpinnerNumberModel(0, Byte.MIN_VALUE, Byte.MAX_VALUE, 1);
            } else if (type.equals(short.class) || type.equals(Short.class)) {
                spinnerModel = new SpinnerNumberModel(0, Short.MIN_VALUE, Short.MAX_VALUE, 1);
            } else if (type.equals(int.class) || type.equals(Integer.class)) {
                spinnerModel = new SpinnerNumberModel(0, Integer.MIN_VALUE, Integer.MAX_VALUE, 1);
            } else if (type.equals(long.class) || type.equals(Long.class)) {
                spinnerModel = new SpinnerNumberModel(0, Long.MIN_VALUE, Long.MAX_VALUE, 1);
            } else if (type.equals(double.class) || type.equals(Double.class)) {
                spinnerModel = new SpinnerNumberModel(0.0, -Double.MAX_VALUE, Double.MAX_VALUE, 1.0);
            } else if (type.equals(float.class) || type.equals(Float.class)) {
                spinnerModel = new SpinnerNumberModel(0.0, -Float.MAX_VALUE, Float.MAX_VALUE, 1.0);
            }
            spinnerModel.setValue(object.getValue());
        } catch (Exception e) {
            Logger.getLogger(NumberProducer.class.getName()).log(Level.FINE, null, e);
        }
        if (spinnerModel != null) {
            final JSpinner spinner = new JSpinner(spinnerModel);
            ((JSpinner.NumberEditor) spinner.getEditor()).getFormat().setMaximumFractionDigits(100);
            ((JSpinner.DefaultEditor) spinner.getEditor()).getTextField().setColumns(
                    SwingDefaults.textFieldDefaultNumberOfChars);
            OhEditorPanel<Number> panel = new OhEditorPanel<Number>(object) {

                @Override
                public Number getValue() {
                    Object ch = spinner.getModel().getValue();
                    if (type.equals(double.class) || type.equals(Double.class)) {
                        return Double.parseDouble(ch.toString());
                    } else if (type.equals(float.class) || type.equals(Float.class)) {
                        return Float.parseFloat(ch.toString());
                    } else if (type.equals(byte.class) || type.equals(Byte.class)) {
                        return Byte.parseByte(ch.toString());
                    } else if (type.equals(short.class) || type.equals(Short.class)) {
                        return Short.parseShort(ch.toString());
                    } else if (type.equals(int.class) || type.equals(Integer.class)) {
                        return Integer.parseInt(ch.toString());
                    } else if (type.equals(long.class) || type.equals(Long.class)) {
                        return Long.parseLong(ch.toString());
                    } else {
                        Logger.getLogger(NumberProducer.class.getName()).warning("Type not supported: " + type);
                    }
                    throw new UnsupportedOperationException("Not supported yet.");
                }
            };
            panel.addLabel(object.getName(), object.getDescription());
            panel.addComponent(spinner, object.getDescription());
            return panel;
        } else {
            return null;
        }
    }
}
