/*
 *  Copyright (C) 2010 Cemagref
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.ohoui.ui.swing.basiccontrols.producers;

import fr.cemagref.ohoui.structure.OhObject;
import fr.cemagref.ohoui.ui.swing.basiccontrols.OhEditorPanel;
import fr.cemagref.ohoui.ui.swing.basiccontrols.OhEditorProducer;
import javax.swing.JCheckBox;
import javax.swing.JComponent;

public class BooleanProducer implements OhEditorProducer<Boolean, JComponent> {

    private static Class[] ty = new Class[]{Boolean.class, boolean.class};

    @Override
    public Class[] getCompatibleTypes() {
        return ty;
    }

    @Override
    public OhEditorPanel<Boolean> getEditor(OhObject<Boolean> object) {
        final JCheckBox checkBox = new JCheckBox(object.getName());
        checkBox.setSelected((Boolean) object.getValue());
        OhEditorPanel editor = new OhEditorPanel<Boolean>(object) {

            @Override
            public Boolean getValue() {
                return checkBox.isSelected();
            }
        };
        editor.addComponent(checkBox, object.getDescription());
        return editor;
    }
}
