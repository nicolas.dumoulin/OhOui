/*
 *  Copyright (C) 2010 Cemagref
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.ohoui.ui.swing.basiccontrols;

import fr.cemagref.ohoui.structure.OhObject;
import fr.cemagref.ohoui.structure.OhObjectComplex;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import org.objenesis.ObjenesisHelper;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

/**
 * Default implementation of editor for an ohObjectComplex, it produces an editor by
 * assembling editors for each field.
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class CommonDefaultObjectEditor extends AbstractOhEditor<Object, JComponent> {

    private static final Logger LOGGER = Logger.getLogger(CommonDefaultObjectEditor.class.getName());
    protected List<AbstractOhEditor> childrenEditors = new ArrayList<AbstractOhEditor>();
    private JPanel pagePanel = new JPanel();

    public CommonDefaultObjectEditor(OhObjectComplex<?> object) {
        super(object);
        panel = new JPanel();
        panel.add(pagePanel, BorderLayout.NORTH);
        pagePanel.setLayout(new BoxLayout(pagePanel, BoxLayout.PAGE_AXIS));
    }

    /**
     * Build the editor from the handled object. This method is intended to be
     * called just after the constructor call.
     */
    public void buildEditor() {
        for (OhObject child : (OhObjectComplex<?>) object) {
            LOGGER.log(Level.FINER, "Building editor for child {0}", object.getName());
            // Editors retrieving for other types
            List<Class> availableTypes = new ArrayList<Class>();
            if (child.getDeclaredType().isInterface())
            availableTypes.add(child.getDeclaredType());
            availableTypes.addAll(Lookup.getDefault().lookupResult(child.getDeclaredType()).allClasses());
            for (Iterator<Class> it  = availableTypes.iterator();it.hasNext();) {
                Class type = it.next();
                if (type.isInterface() || Modifier.isAbstract(type.getModifiers())) {
                    it.remove();
                }
            }
            // TODO filter added types to retain only instanciable types
            if (child.getValue() == null) {
                // object has null value, so building specific panel for instanciating it
                OhEditorPanel editor = new OhEditorPanel(child) {

                    @Override
                    public Object getValue() {
                        return null;
                    }
                };
                editor.setEnabled(false);
                editor.addLabel(child.getName(), child.getDescription());
                if (availableTypes.isEmpty()) {
                    editor.addComponent(new JLabel("null"), "No type have been found to affect a new instance in this variable.");
                } else {
                    editor.addComponent(ControlsFactory.makeButton("general/New16",
                            new EditTypeAction(this, editor, child, availableTypes.toArray(new Class[]{}), childrenEditors.size()),
                            "", "New"), "Create a new instance");
                }
                pagePanel.add((JComponent) editor.getPanel());
                childrenEditors.add(editor);
            } else {
                // adding provided editor
                addEditorFor(child);
            }
        }
    }

    protected final void addPanelAt(JComponent component, int index) {
        pagePanel.add(component, index);
    }

    protected final void registerEditor(AbstractOhEditor<Object, JComponent> editor) {
        childrenEditors.add(editor);
    }

    protected void addEditorFor(OhObject object, int index) {
        AbstractOhEditor<Object, JComponent> editor = EditorsProvider.createEditorFor(object);
        if (editor != null) {
            // TODO add button for reinstanciate object
            addPanelAt((JComponent) editor.getPanel(), index);
            registerEditor(editor);
        } else {
            // TODO it shouldn't occur, but as collections don't have editor, it can
            addPanelAt(new JPanel(), index);
        }
    }

    private void addEditorFor(OhObject object) {
        addEditorFor(object, childrenEditors.size());
        pagePanel.revalidate();
    }

    @Override
    public boolean isEnabled() {
        return panel.isEnabled();
    }

    @Override
    protected void enableComponent(boolean enabled) {
        panel.setEnabled(enabled);
    }

    @Override
    protected void recursivelyEnableComponent(JComponent component, boolean enabled) {
        for (Component subComponent : component.getComponents()) {
            if (subComponent instanceof JComponent) {
                JComponent subJComponent = (JComponent) subComponent;
                recursivelyEnableComponent(subJComponent, enabled);
            }
            subComponent.setEnabled(enabled);
        }
    }

    @Override
    public Object getValue() {
        // update objects values for each registered editors
        for (AbstractOhEditor editor : childrenEditors) {
            if (editor.isEnabled()) {
                try {
                    editor.getOhObject().setValue(editor.getValue());
                } catch (IllegalArgumentException ex) {
                    Exceptions.attachMessage(ex, "Error while trying to update the field " + editor.getOhObject().getName() + " in " + getOhObject().getName());
                    Exceptions.printStackTrace(ex);
                } catch (IllegalAccessException ex) {
                    Exceptions.attachMessage(ex, "Error while trying to update the field " + editor.getOhObject().getName() + " in " + getOhObject().getName());
                    Exceptions.printStackTrace(ex);
                }
            }
        }
        return getOhObject().getValue();
    }

    /**
     * Action to edit the type of an object and update the editor.
     */
    private final class EditTypeAction implements ActionListener {

        private CommonDefaultObjectEditor container;
        private OhEditorPanel currentObjectEditor;
        private OhObject ohObject;
        private Class[] availableTypes;
        private int editorIndex;

        public EditTypeAction(CommonDefaultObjectEditor container, OhEditorPanel currentObjectEditor, OhObject ohObject, Class[] availableTypes, int editorIndex) {
            this.container = container;
            this.currentObjectEditor = currentObjectEditor;
            this.ohObject = ohObject;
            this.availableTypes = availableTypes;
            this.editorIndex = editorIndex;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            Class chosenType;
            if (availableTypes.length == 1) {
                chosenType = availableTypes[0];
            } else {
                // bring a dialog to choose the type
                chosenType = (Class) JOptionPane.showInputDialog(panel,
                        "Choose the type you want to use", "Choose a type", JOptionPane.QUESTION_MESSAGE, null,
                        availableTypes, availableTypes[0]);
            }
            if (chosenType != null) {
                try {
                    // instanciate
                    ohObject.setValue(ObjenesisHelper.newInstance(chosenType));
                    // remove current editor
                    childrenEditors.remove(currentObjectEditor);
                    pagePanel.remove(editorIndex);
                    // build the corresponding panel and add the panel in place
                    container.addEditorFor(ohObject, editorIndex);
                    pagePanel.revalidate();
                } catch (IllegalArgumentException ex) {
                    Exceptions.printStackTrace(ex);
                } catch (IllegalAccessException ex) {
                    Exceptions.printStackTrace(ex);
                }
            }
        }
    }
}
