package fr.cemagref.ohoui.ui.swing.basiccontrols;

import fr.cemagref.ohoui.structure.OhObject;
import java.awt.Component;
import java.awt.FlowLayout;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Default implementation for producing a Panel from an ohObject.
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public abstract class OhEditorPanel<T> extends AbstractOhEditor<T, JComponent> {

    protected OhEditorPanel(OhObject<T> object) {
        super(object);
        panel = new JPanel();
        panel.setLayout(new FlowLayout(FlowLayout.LEFT));
        this.object = object;
    }

    public JLabel addLabel(String label, String tooltip) {
        JLabel jlabel = new JLabel(label);
        jlabel.setPreferredSize(SwingDefaults.labelMinimumSize);
        if ((tooltip != null) && (!"".equals(tooltip))) {
            jlabel.setToolTipText(tooltip);
        }
        panel.add(jlabel);
        return jlabel;
    }

    public void addComponent(JComponent component, String tooltip) {
        component.setAlignmentX(SwingDefaults.labelAlignment);
        if (!"".equals(tooltip)) {
            component.setToolTipText(tooltip);
        }
        panel.add(component);
    }

    @Override
    public boolean isEnabled() {
        return panel.isEnabled();
    }

    @Override
    protected void enableComponent(boolean enabled) {
        panel.setEnabled(enabled);
    }

    @Override
    protected void recursivelyEnableComponent(JComponent component, boolean enabled) {
        for (Component subComponent : component.getComponents()) {
            if (subComponent instanceof JComponent) {
                JComponent subJComponent = (JComponent) subComponent;
                recursivelyEnableComponent(subJComponent, enabled);
            }
            subComponent.setEnabled(enabled);
        }
    }
}
