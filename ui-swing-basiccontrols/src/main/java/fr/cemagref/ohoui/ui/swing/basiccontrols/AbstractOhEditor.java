/*
 *  Copyright (C) 2010 Cemagref
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.ohoui.ui.swing.basiccontrols;

import fr.cemagref.ohoui.structure.OhObject;

/**
 * Defines common behavior for editors for an object.
 * 
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 * @param <T> The type of the edited object
 * @param <P> The type of graphical component used
 */
public abstract class AbstractOhEditor<T, P> {

    protected OhObject<T> object;
    protected P panel;

    public AbstractOhEditor(OhObject object) {
        this.object = object;
    }

    /**
     * Gets the object edited. This method doesn't achieve any update relative
     * to the edited value.
     * @return
     */
    public OhObject<T> getOhObject() {
        return object;
    }

    public void setOhObject(OhObject<T> object) {
        this.object = object;
    }

    /**
     * Returns the value up-to-date of the edited object.
     * @return The object edited
     */
    public abstract T getValue();

    public P getPanel() {
        return panel;
    }

    public final void setEnabled(boolean enabled) {
        enableComponent(enabled);
        // recursively enable/disable container and its enclosing container and
        // components
        recursivelyEnableComponent(panel, enabled);
    }

    public abstract boolean isEnabled();

    protected abstract void enableComponent(boolean enabled);

    protected abstract void recursivelyEnableComponent(P container, boolean enabled);

}
