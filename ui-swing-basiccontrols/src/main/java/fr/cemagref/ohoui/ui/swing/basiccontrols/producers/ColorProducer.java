/*
 *  Copyright (C) 2010 Cemagref
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.ohoui.ui.swing.basiccontrols.producers;

import fr.cemagref.ohoui.structure.OhObject;
import fr.cemagref.ohoui.ui.swing.basiccontrols.OhEditorPanel;
import fr.cemagref.ohoui.ui.swing.basiccontrols.OhEditorProducer;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JComponent;

public class ColorProducer implements OhEditorProducer<Color, JComponent> {

    private static Class[] ty = new Class[]{Color.class};

    @Override
    public Class[] getCompatibleTypes() {
        return ty;
    }

    @Override
    public OhEditorPanel getEditor(OhObject<Color> object) {
        return new ColorPanel(object);
    }

    private class ColorPanel extends OhEditorPanel<Color> implements ActionListener {

        private JButton editButton;
        private Color color;

        public ColorPanel(OhObject<Color> object) {
            super(object);
            editButton = new JButton();
            editButton.setBackground((Color) object.getValue());
            addLabel(object.getName(), object.getDescription());
            addComponent(editButton, object.getDescription());
            editButton.addActionListener(this);
        }

        @Override
        public Color getValue() {
            return color;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            Color answer = JColorChooser.showDialog(this.getPanel(), "Choose your color", color);
            if (answer != null) {
                color = answer;
                editButton.setBackground(color);
            }
        }
    }
}
