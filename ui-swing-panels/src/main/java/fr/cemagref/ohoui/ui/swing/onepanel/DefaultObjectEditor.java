/*
 *  Copyright (C) 2010 Cemagref
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.ohoui.ui.swing.onepanel;

import fr.cemagref.ohoui.structure.OhObject;
import fr.cemagref.ohoui.structure.OhObjectComplex;

import fr.cemagref.ohoui.ui.swing.basiccontrols.AbstractOhEditor;
import fr.cemagref.ohoui.ui.swing.basiccontrols.CommonDefaultObjectEditor;
import fr.cemagref.ohoui.ui.swing.basiccontrols.EditorsProvider;
import javax.swing.JComponent;

/**
 * Default implementation of editor for an ohObjectComplex, it produces an editor by
 * assembling editors for each field.
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class DefaultObjectEditor extends CommonDefaultObjectEditor {

    private OhPanel container;

    public DefaultObjectEditor(OhPanel container, OhObjectComplex<?> object) {
        super(object);
        this.container = container;
    }

    @Override
    protected void addEditorFor(OhObject object, int index) {
        AbstractOhEditor<Object, JComponent> editor = EditorsProvider.createEditorFor(object);
        if (editor != null) {
            // TODO add button for reinstanciate object
            addPanelAt((JComponent) editor.getPanel(), index);
        } else {
            if (!(object instanceof OhObjectComplex)) {
                throw new UnsupportedOperationException("This case has not been planned.");
            }
            editor = new ObjectComplexEditorPanel((OhObjectComplex) object);
            ((ObjectComplexEditorPanel)editor).setContainer(container);
            addPanelAt(editor.getPanel(), index);
        }
        registerEditor(editor);
    }
}
