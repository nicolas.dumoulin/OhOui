/*
 *  Copyright (C) 2011 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.ohoui.ui.swing.onepanel;

import fr.cemagref.ohoui.reflect.DefaultObjectIntrospector;
import fr.cemagref.ohoui.reflect.IntrospectionException;
import fr.cemagref.ohoui.reflect.OhOuiContext;
import fr.cemagref.ohoui.structure.OhObject;
import fr.cemagref.ohoui.structure.OhObjectCollection;
import fr.cemagref.ohoui.structure.OhObjectComplex;
import fr.cemagref.ohoui.ui.swing.basiccontrols.AbstractOhEditor;
import fr.cemagref.ohoui.ui.swing.basiccontrols.EditorsProvider;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * This class provides a panel for editing an object.
 * Each field will generate a field in a vertical form. Complex fields like
 * collections or non-standard type (numbers, string, …) will generate a button
 * for opening a new panel and editing this sub-object.
 * If needed, a menu can display the path in the data structure of the object
 * that is currently edited, that allows the user to come back to higher level
 * node in the data structure.
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class OhPanel extends JPanel {

    private JPanel editorsPanel;
    private JComponent pathPanel;
    private Map<Object, AbstractOhEditor<Object, JComponent>> editorsMap;
    private CardLayout editorsCardLayout;
    private final static String EMPTY_EDITOR_CODE = "empty";
    private AbstractOhEditor<Object, JComponent> rootEditor;

    /**
     * Load an object for editing.
     * @param title The name of the object to edit used for displaying
     * @param o the object to edit
     * @throws IntrospectionException
     */
    public void loadObject(String title, Object o) throws IntrospectionException {
        this.editorsMap = new HashMap<Object, AbstractOhEditor<Object, JComponent>>();
        this.editorsCardLayout = new CardLayout();
        this.editorsPanel = new JPanel(editorsCardLayout);
        editorsPanel.add(new JPanel(), EMPTY_EDITOR_CODE);
        pathPanel = new JPanel();
        pathPanel.setLayout(new BoxLayout(pathPanel, BoxLayout.LINE_AXIS));
        this.setLayout(new BorderLayout());
        this.add(pathPanel, BorderLayout.NORTH);
        this.add(editorsPanel);

        OhOuiContext ohOuiContext = new OhOuiContext();
        OhObjectComplex ohObjectComplex = DefaultObjectIntrospector.introspect(title, o, ohOuiContext);
        rootEditor = selectObject(ohObjectComplex);
    }

    private String getCode(OhObjectComplex selectedObject) {
        return Integer.toString(selectedObject.hashCode());
    }

    /**
     * Select an object for editing in the tree of the datastructure
     * @param selectedObject
     */
    public final AbstractOhEditor<Object, JComponent> selectObject(OhObjectComplex selectedObject) {
        // find the path of the edited object from the root and update the path display
        OhObjectComplex current = selectedObject;
        Dimension dimension = new Dimension(40, 40);
        pathPanel.removeAll();
        JLabel label = new JLabel(current.getName());
        label.setPreferredSize(dimension);
        pathPanel.add(label);
        current = current.getParent();
        while (current != null) {
            pathPanel.add(Box.createRigidArea(new Dimension(5, 0)), 0);
            JButton pathButton = new JButton(current.getName());
            pathButton.setPreferredSize(dimension);
            pathButton.setContentAreaFilled(false);
            pathButton.addActionListener(new PathActionListener(current));
            pathPanel.add(pathButton, 0);
            current = current.getParent();
        }
        pathPanel.add(new JLabel("Editing: "), 0);
        pathPanel.repaint();
        // display the editor
        boolean firstAdd = !editorsMap.containsKey(selectedObject);
        AbstractOhEditor<Object, JComponent> editor = getEditorFor(selectedObject);
        if (editor != null) {
            if (firstAdd) {
                editorsPanel.add(editor.getPanel(), getCode(selectedObject));
            }
            editorsCardLayout.show(editorsPanel, getCode(selectedObject));
        } else {
            editorsCardLayout.show(editorsPanel, EMPTY_EDITOR_CODE);
        }
        return editor;
    }

    public final AbstractOhEditor<Object, JComponent> getEditorFor(OhObject selectedObject) {
        AbstractOhEditor<Object, JComponent> editor = editorsMap.get(selectedObject);
        if (editor == null) {
            // retrieve editor and put it in cache
            if (EditorsProvider.hasEditorFor(selectedObject)) {
                editor = EditorsProvider.createEditorFor(selectedObject);
            } else if (selectedObject instanceof OhObjectCollection) {
                // TODO bring an editor with a list and add/remove buttons
            } else if (selectedObject instanceof OhObjectComplex) {
                editor = new DefaultObjectEditor(this, (OhObjectComplex<?>) selectedObject);
                ((DefaultObjectEditor) editor).buildEditor();
            } else {
                // TODO pay attention if object value is null ?
                editor = EditorsProvider.createEditorFor(selectedObject);
            }
            editorsMap.put(selectedObject, editor);
        }
        return editor;
    }

    public void applyChanges() {
        rootEditor.getValue();
    }

    private class PathActionListener implements ActionListener {

        private final OhObjectComplex current;

        public PathActionListener(OhObjectComplex current) {
            this.current = current;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            selectObject(current);
        }
    }
}
