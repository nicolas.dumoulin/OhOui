/*
 *  Copyright (C) 2011 Cemagref
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.cemagref.ohoui.ui.swing.onepanel;

import fr.cemagref.ohoui.structure.OhObjectComplex;
import fr.cemagref.ohoui.ui.swing.basiccontrols.AbstractOhEditor;
import fr.cemagref.ohoui.ui.swing.basiccontrols.OhEditorPanel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComponent;

/**
 *
 * @author Nicolas Dumoulin <nicolas.dumoulin@cemagref.fr>
 */
public class ObjectComplexEditorPanel extends OhEditorPanel<Object> implements ActionListener {

    private OhPanel container;

    public ObjectComplexEditorPanel(OhObjectComplex<Object> object) {
        super(object);
        addLabel(object.getName(), object.getDescription());
        JButton button = new JButton("Edit\u2026");
        button.addActionListener(this);
        addComponent(button, object.getDescription());
    }

    public void setContainer(OhPanel container) {
        this.container = container;
    }

    @Override
    public Object getValue() {
        AbstractOhEditor<Object, JComponent> editor = container.getEditorFor(super.object);
        if (editor != null) {
            return editor.getValue();
        } else {
            return null;
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.container.selectObject((OhObjectComplex) super.object);
    }
}
