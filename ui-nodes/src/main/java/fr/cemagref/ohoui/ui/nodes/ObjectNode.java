package fr.cemagref.ohoui.ui.nodes;

import fr.cemagref.ohoui.structure.OhObject;
import fr.cemagref.ohoui.structure.OhObjectComplex;
import org.openide.nodes.Children;
import org.openide.nodes.Node;

public class ObjectNode extends AbstractObjectNode<OhObjectComplex> {

    public ObjectNode(OhObjectComplex object) {
        super(object, new ObjectChildren(object));
    }
}

class ObjectChildren extends Children.Keys<OhObject> {

    public ObjectChildren(OhObjectComplex objectComplex) {
        setKeys(objectComplex.getChildren());
    }

    @Override
    protected Node[] createNodes(OhObject ohObject) {
        Node node;
        if (ohObject instanceof OhObjectComplex) {
            node = new ObjectNode((OhObjectComplex) ohObject);
        } else {
            node = new ObjectLeaf(ohObject);
        }
        return new Node[]{node};
    }
}
