/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.cemagref.ohoui.ui.nodes;

import java.lang.reflect.InvocationTargetException;
import fr.cemagref.ohoui.structure.OhObject;
import org.openide.nodes.PropertySupport;

/**
 *
 * @author dumoulin
 */
public class ValueProperty extends PropertySupport.ReadOnly<String> {

    public final static String PROP_VALUE = "value";
    private OhObject ohObject;

    public ValueProperty() {
        super(PROP_VALUE, String.class, "Value", "Value of the object");
    }

    public ValueProperty(OhObject ohObject) {
        this();
        this.ohObject = ohObject;
    }

    @Override
    public String getValue() throws IllegalAccessException, InvocationTargetException {
        if (this.ohObject != null) {
            return this.ohObject.getValue().toString();
        } else {
            return "Empty";
        }
    }
}
