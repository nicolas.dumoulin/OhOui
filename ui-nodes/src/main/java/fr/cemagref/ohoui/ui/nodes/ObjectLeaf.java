/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.cemagref.ohoui.ui.nodes;

import fr.cemagref.ohoui.structure.OhObject;
import org.openide.nodes.Children;

/**
 *
 * @author dumoulin
 */
public class ObjectLeaf extends AbstractObjectNode<OhObject> {

    public ObjectLeaf(OhObject object) {
        super(object, Children.LEAF);
    }
}
