package fr.cemagref.ohoui.ui.nodes;

import fr.cemagref.ohoui.structure.OhObject;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Sheet;
import org.openide.util.lookup.Lookups;

public class AbstractObjectNode<O extends OhObject> extends AbstractNode {

    public AbstractObjectNode(O object, Children children) {
        super(children, Lookups.singleton(object));
        setDisplayName(object.getName());
    }

    public O getObject() {
        return (O) getLookup().lookup(OhObject.class);
    }

    @Override
    protected Sheet createSheet() {
        Sheet s = super.createSheet();
        Sheet.Set ss = s.get(Sheet.PROPERTIES);
        if (ss == null) {
            ss = Sheet.createPropertiesSet();
            s.put(ss);
        }
        ss.put(new ValueProperty(getObject()));
        return s;
    }
}
